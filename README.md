# README #
By Frieder Wittmann, PhD, Sensory-Motor Systems Lab, ETH Zurich, frieder.wittmann@gmail.com

For now this repository is only intended for the reviewers of our IRCRA paper with the title
AN OPEN-SOURCE LOW-COST INSTRUMENTED HANGBOARD FOR CLIMBING SPECIFIC ASSESSMENT AND TRAINING OF FINGER FLEXORS
by Wittmann, F, Feldmann A, Lehmann R, Simpson, R, Erlacher D and Wolf, P

If you are not one of the reviewers or the author, please inform me by sending an email to frieder.wittmann@gmail.com

As of now we only provide the assembly instructions for the hangboard, which can be found here:
https://bitbucket.org/friederw/eth_hangboard/downloads/

as well as installation files for Windows 10, which can be found here:
https://drive.google.com/file/d/1KLYw9D9QiRdoNn4U9ckMASfhvO9TwH_e/view?usp=sharing

Source code, will be cleaned up and published before the 5th IRCRA conference in November 2021.

